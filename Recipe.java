package SeasonalFoods;

public final class Recipe
{
    private String tags[] = new String[20];
    private String ingredients[]=new String[20];
    private String name;
    private String instructions; 
    private int noOfIngredients =0;
    Recipe(){}
    Recipe(String nName,String[] nIngredients, String nInstructions)
    {
        noOfIngredients = calcNoOfIngredients(nIngredients);
        setName(nName);
        setIngredients(nIngredients,noOfIngredients);
        setInstructions(nInstructions);       
    }
    
    Recipe getRecipe()
    {
        return this;
    }
    
    void setName(String newName)
    {
        name = newName;
    }    
    String getName()
    {
        return name;
    }
    
    void setInstructions(String newInstructions)
    {
        instructions = newInstructions;
    }    
    String getInstructions()
    {
        return instructions;
    }
    
    void setIngredients(String[] newIngredient, int i)
    {
        for(int j=0;j<i;j++)
        {
            ingredients[j] = newIngredient[j];
        }         
    }    
    String[] getIngredients()
    {
        return ingredients;
    }
    
    void setNoOfIngredients(int n)
    {
        noOfIngredients = n;
    }
    int getNoOfIngredients()
    {
        return noOfIngredients;
    }
    int calcNoOfIngredients(String nIngredients[])
    {
        int count=0;
        for (String str : nIngredients) 
        {
            if ( str != null ) count++;
        }           
        return count;
    }
    
    
}