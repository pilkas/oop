package SeasonalFoods;
import javax.swing.DefaultListModel;

import java.io.File;
import javax.swing.JFileChooser;

public class AddRecipe extends javax.swing.JFrame {    
    private String Ingredient[] = new String[20];
    private String instructions;
    private String name;
    private int quantity;  
    private int i=0;   
    
    public AddRecipe() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        ingredientList = new javax.swing.JList<>();
        addInstructionsLabel = new javax.swing.JLabel();
        saveRecipeButton = new javax.swing.JButton();
        addIngredient = new javax.swing.JTextField();
        addIngredientButton = new javax.swing.JButton();
        recipeName = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        instructionArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        ingredientList.setModel(new DefaultListModel());
        jScrollPane1.setViewportView(ingredientList);

        addInstructionsLabel.setText("Add instructions");

        saveRecipeButton.setText("Save");
        saveRecipeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveRecipeButtonActionPerformed(evt);
            }
        });

        addIngredient.setToolTipText("Enter an ingredient");
        addIngredient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addIngredientActionPerformed(evt);
            }
        });

        addIngredientButton.setText("Add");
        addIngredientButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addIngredientButtonActionPerformed(evt);
            }
        });

        recipeName.setText("Recipe name");

        instructionArea.setColumns(20);
        instructionArea.setRows(5);
        jScrollPane2.setViewportView(instructionArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(recipeName)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(addIngredient)
                        .addGap(33, 33, 33)
                        .addComponent(addIngredientButton, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(addInstructionsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(saveRecipeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(addInstructionsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(saveRecipeButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(recipeName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(addIngredientButton)
                            .addComponent(addIngredient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void saveRecipeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveRecipeButtonActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Select or create new folder for the recipes");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
       
     // System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
     // System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
    } else {
      System.out.println("No Selection ");
    }
        
        File file = chooser.getSelectedFile();
        String fullpath = file.getPath();
        
        String finalpath = fullpath.replace('\\','/');
                    
        System.out.println("Fullpath: " + finalpath);
        
        instructions = instructionArea.getText();
        name = recipeName.getText();
        Write n = new Write();
        Recipe a = new Recipe(name,Ingredient,instructions);
        n.writeToFile(a,finalpath);       
        this.setVisible(false);        

    }//GEN-LAST:event_saveRecipeButtonActionPerformed

    private void addIngredientButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addIngredientButtonActionPerformed
        // TODO add your handling code here:
        Ingredient[i] = addIngredient.getText();
        ((DefaultListModel)ingredientList.getModel()).addElement(Ingredient[i]);
        i++;
        addIngredient.setText(""); 
    }//GEN-LAST:event_addIngredientButtonActionPerformed

    private void addIngredientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addIngredientActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addIngredientActionPerformed

 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField addIngredient;
    private javax.swing.JButton addIngredientButton;
    private javax.swing.JLabel addInstructionsLabel;
    private javax.swing.JList<String> ingredientList;
    private javax.swing.JTextArea instructionArea;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField recipeName;
    private javax.swing.JButton saveRecipeButton;
    // End of variables declaration//GEN-END:variables
}
