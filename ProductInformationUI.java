package SeasonalFoods;

public class ProductInformationUI extends javax.swing.JFrame {

    /**
     * Creates new form ProductInformationUI
     */
    
    public ProductInformationUI()
    {
        
        initComponents(); 
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Back1 = new javax.swing.JButton();
        jComboSeasons = new javax.swing.JComboBox<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextFish = new javax.swing.JTextArea();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTextFruits = new javax.swing.JTextArea();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTextVeggies = new javax.swing.JTextArea();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTextMeat = new javax.swing.JTextArea();
        jLabelFish = new javax.swing.JLabel();
        jLabelFruits = new javax.swing.JLabel();
        jLabelVeggies = new javax.swing.JLabel();
        jLabelMeat = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Product Information");
        setResizable(false);

        Back1.setText("Back");
        Back1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Back1ActionPerformed(evt);
            }
        });

        jComboSeasons.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select a season", "Spring", "Summer", "Autumn", "Winter" }));
        jComboSeasons.setPreferredSize(new java.awt.Dimension(30, 30));
        jComboSeasons.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboSeasonsActionPerformed(evt);
            }
        });

        jTextFish.setEditable(false);
        jTextFish.setColumns(20);
        jTextFish.setRows(5);
        jScrollPane5.setViewportView(jTextFish);

        jTextFruits.setEditable(false);
        jTextFruits.setColumns(20);
        jTextFruits.setRows(5);
        jScrollPane6.setViewportView(jTextFruits);

        jTextVeggies.setEditable(false);
        jTextVeggies.setColumns(20);
        jTextVeggies.setRows(5);
        jScrollPane7.setViewportView(jTextVeggies);

        jTextMeat.setEditable(false);
        jTextMeat.setColumns(20);
        jTextMeat.setRows(5);
        jScrollPane8.setViewportView(jTextMeat);

        jLabelFish.setText("Fish and seafood");

        jLabelFruits.setText("Fruits");

        jLabelVeggies.setText("Vegetables");

        jLabelMeat.setText("Meat");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jComboSeasons, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Back1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelMeat)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelFruits)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelVeggies)
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelFish)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jComboSeasons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelFruits)
                    .addComponent(jLabelVeggies))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelMeat)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelFish)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Back1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setBounds(0, 0, 530, 545);
    }// </editor-fold>//GEN-END:initComponents

    private void Back1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Back1ActionPerformed
    this.setVisible(false);
    //a.setVisible(true);
    }//GEN-LAST:event_Back1ActionPerformed

    private void jComboSeasonsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboSeasonsActionPerformed
        // TODO add your handling code here: 
        String itemText = (String)jComboSeasons.getSelectedItem(); // Picking season
            
        String springFruits ="Banana\n" +"Grapefruit\n" +"Pomegranate\n" +"Rhubarb";
        String summerFruits = "Apricot\n" +"Banana\n" +"Blackcurrants\n" +"Gooseberry\n" +"Nectarine\n" +"Pomegranate\n" +"Raspberry\n" +"Redcurrant\n" +"Strawberry\n" +"Watermelon";
        String autumnFruits = "Apple\n" +"Apricot\n" +"Banana\n" +"Blackberry\n" +"Cranberry\n" +"Fig\n" +"Pear\n" +"Plum\n" +"Pomegranate";
        String winterFruits = "Apple\n" +"Banana\n" +"Bramley Apple\n" +"Clementine\n" +"Grapefruit\n" +"Lemon\n" +"Pear\n" +"Pomegranate\n" +"Rhubarb";
        
        String springVeggies = "Cabbage\n" +"Cauliflower\n" +"Celeriac\n" +"New potatoes\n" +"Pepper\n" +"Potato\n" +"Radicchio\n" +"Spinach\n" +"Spring greens\n" +"Watercress";
        String summerVeggies = "Asparagus\n" +"Aubergine\n" +"Basil\n" +"Beetroot\n" +"Cabbage\n" +"Carrot\n" +"Courgette\n" +"Garlic\n" +"Globe artichoke\n" +"Lamb's lettuce\n" +"Lettuce\n" +"New potatoes\n" +"Peas\n" +"Pepper\n" +"Potato\n" +"Radicchio\n" +"Radish\n" +"Runner bean\n" +"Spinach\n" +"Tomato\n" +"Watercress";
        String autumnVeggies = "Aubergine\n" +"Beetroot\n" +"Broccoli\n" +"Brussels sprouts\n" +"Cabbage\n" +"Celeriac\n" +"Celery\n" +"Garlic\n" +"Globe artichoke\n" +"Lamb's lettuce\n" +"Leek\n" +"Lettuce\n" +"Parsnip\n" +"Peas\n" +"Pepper\n" +"Pumpkin\n" +"Radicchio\n" +"Radish\n" +"Runner bean\n" +"Sweet potato\n" +"Swiss chard\n" +"Turnip\n" +"Tomato";
        String winterVeggies = "Beetroot\n" +"Brussels sprouts\n" +"Cabbage\n" +"Cauliflower\n" +"Celeriac\n" +"Celery\n" +"Jerusalem artichoke\n" +"Leek\n" +"Parsnip\n" +"Radicchio\n" +"Swede\n" +"Sweet potato\n" +"Turnip";
        
        String springMeat = "Spring\n" +"Lamb\n" +"Pork\n" +"Spring lamb";
        String summerMeat = "Lamb\n" +"Pork\n" +"Spring lamb";
        String autumnMeat = "Duck\n" +"Goose\n" +"Lamb\n" +"Pork";
        String winterMeat = "Goose\n" +"Lamb\n" +"Pork\n" +"Venison";
        
        String springFish = "Cod\n" +"Crab\n" +"Halibut\n" +"Salmon";
        String summerFish = "Crab\n" +"Halibut\n" +"Kipper\n" +"Mackerel\n" +"Salmon\n" +"Tuna\n" +"Whiting";
        String autumnFish = "Crab\n" +"Mackerel\n" +"Mussels\n" +"Whiting";
        String winterFish = "Mussels\n" +"Whiting";
        
        
        if("Spring".equals(itemText)){
            jTextFruits.setText(springFruits);
            jTextVeggies.setText(springVeggies);
            jTextMeat.setText(springMeat);
            jTextFish.setText(springFish);
        }               
        else if("Summer".equals(itemText)){
            jTextFruits.setText(summerFruits);
            jTextVeggies.setText(summerVeggies);
            jTextMeat.setText(summerMeat);
            jTextFish.setText(summerFish);
        }  
        else if("Autumn".equals(itemText)){
            jTextFruits.setText(autumnFruits);
            jTextVeggies.setText(autumnVeggies);
            jTextMeat.setText(autumnMeat);
            jTextFish.setText(autumnFish);
        } 
        else if("Winter".equals(itemText)){
            jTextFruits.setText(winterFruits);
            jTextVeggies.setText(winterVeggies);
            jTextMeat.setText(winterMeat);
            jTextFish.setText(winterFish);
        } 
        
        else if("Select a season".equals(itemText))
        {
            jTextFruits.setText("");
            jTextVeggies.setText("");
            jTextMeat.setText("");
            jTextFish.setText("");
        }
    }//GEN-LAST:event_jComboSeasonsActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Back1;
    private javax.swing.JComboBox<String> jComboSeasons;
    private javax.swing.JLabel jLabelFish;
    private javax.swing.JLabel jLabelFruits;
    private javax.swing.JLabel jLabelMeat;
    private javax.swing.JLabel jLabelVeggies;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTextArea jTextFish;
    private javax.swing.JTextArea jTextFruits;
    private javax.swing.JTextArea jTextMeat;
    private javax.swing.JTextArea jTextVeggies;
    // End of variables declaration//GEN-END:variables
}
