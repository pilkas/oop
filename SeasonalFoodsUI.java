package SeasonalFoods;
import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;

public class SeasonalFoodsUI extends javax.swing.JFrame {
    int iYear = 2017;
    int iMonth = Calendar.JANUARY; // 0 (months begin with 0)
    int iDay = 1;
    String currDate = "";  
    SimpleDateFormat format1 = new SimpleDateFormat("yyyy/MM/dd");  
    Calendar mycal = new GregorianCalendar(iYear, iMonth, iDay);      
    int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);  // Get the number of days in that month
    String ingredient;
    private String fullpath;
    
    public SeasonalFoodsUI() {
        initComponents();
        setSeason();      

    }
    private void setSeason()
    {
        currDate = format1.format(mycal.getTime());
        cDateLabel.setText("Current date: "+ currDate);
        int month = mycal.get(Calendar.MONTH);        
        switch (month)
        {
            case 11:
            case 0:
            case 1:
                cSeasonLabel.setText("Current season: " + "Winter");
                break;
            case 2:
            case 3:
            case 4:
                cSeasonLabel.setText("Current season: " + "Spring");
                break;
            case 5:
            case 6:
            case 7:
                cSeasonLabel.setText("Current season: " + "Summer");
                break;
            case 8:
            case 9:
            case 10:
                cSeasonLabel.setText("Current season: " + "Autumn");
                break;
            default:
                break;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Searchfield = new javax.swing.JTextField();
        CheckBoxPanel = new javax.swing.JPanel();
        jCheckBox3 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox5 = new javax.swing.JCheckBox();
        jCheckBox4 = new javax.swing.JCheckBox();
        cDateLabel = new javax.swing.JLabel();
        DatePanel = new javax.swing.JPanel();
        jComboBoxMonth = new javax.swing.JComboBox<>();
        jComboBoxYear = new javax.swing.JComboBox<>();
        jComboBoxDay = new javax.swing.JComboBox<>();
        setDate = new javax.swing.JButton();
        selectDateButton = new javax.swing.JButton();
        addRecipeButton = new javax.swing.JButton();
        cSeasonLabel = new javax.swing.JLabel();
        prodInfoButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        recipeList = new javax.swing.JList<>();
        ForDeletion = new javax.swing.JLabel();
        SearchButton = new javax.swing.JButton();
        AllergyButton = new javax.swing.JButton();
        AllergyPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        AllergyField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        CAllerygyButton = new javax.swing.JButton();
        AddToAllleListButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        AllergyProductArea = new javax.swing.JTextArea();
        GetRecipesBu = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SeasonalFoods");
        setLocation(new java.awt.Point(400, 0));
        setPreferredSize(new java.awt.Dimension(800, 600));
        setResizable(false);
        setSize(new java.awt.Dimension(800, 600));

        Searchfield.setBackground(new java.awt.Color(255, 255, 255));
        Searchfield.setForeground(new java.awt.Color(0, 0, 0));
        Searchfield.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jCheckBox3.setText("Vegan");

        jCheckBox2.setText("Vegetarian");

        jCheckBox1.setText("Dairy free");

        jCheckBox5.setText("Raw");

        jCheckBox4.setText("Easy to make");
        jCheckBox4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout CheckBoxPanelLayout = new javax.swing.GroupLayout(CheckBoxPanel);
        CheckBoxPanel.setLayout(CheckBoxPanelLayout);
        CheckBoxPanelLayout.setHorizontalGroup(
            CheckBoxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CheckBoxPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jCheckBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        CheckBoxPanelLayout.setVerticalGroup(
            CheckBoxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CheckBoxPanelLayout.createSequentialGroup()
                .addGroup(CheckBoxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox3)
                    .addComponent(jCheckBox2)
                    .addComponent(jCheckBox1)
                    .addComponent(jCheckBox5)
                    .addComponent(jCheckBox4))
                .addGap(0, 7, Short.MAX_VALUE))
        );

        cDateLabel.setText("Current date:");

        DatePanel.setForeground(java.awt.Color.lightGray);

        jComboBoxMonth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Month", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));
        jComboBoxMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxMonthActionPerformed(evt);
            }
        });

        jComboBoxYear.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Year", "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999" }));
        jComboBoxYear.setToolTipText("");
        jComboBoxYear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxYearActionPerformed(evt);
            }
        });

        jComboBoxDay.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Day" }));
        jComboBoxDay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDayActionPerformed(evt);
            }
        });

        setDate.setFont(new java.awt.Font("Cantarell", 0, 8)); // NOI18N
        setDate.setText("x");
        setDate.setToolTipText("Save date");
        setDate.setIconTextGap(0);
        setDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setDateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout DatePanelLayout = new javax.swing.GroupLayout(DatePanel);
        DatePanel.setLayout(DatePanelLayout);
        DatePanelLayout.setHorizontalGroup(
            DatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DatePanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(DatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jComboBoxDay, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxYear, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxMonth, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(setDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        DatePanelLayout.setVerticalGroup(
            DatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DatePanelLayout.createSequentialGroup()
                .addGroup(DatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(DatePanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jComboBoxYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(setDate, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBoxMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jComboBoxDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        DatePanel.setVisible(false);

        selectDateButton.setText("Select date");
        selectDateButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        selectDateButton.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        selectDateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectDateButtonActionPerformed(evt);
            }
        });

        addRecipeButton.setText("Add recipe");
        addRecipeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addRecipeButtonActionPerformed(evt);
            }
        });

        cSeasonLabel.setText("Current season:");

        prodInfoButton.setText("Product Info");
        prodInfoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prodInfoButtonActionPerformed(evt);
            }
        });

        recipeList.setModel(new DefaultListModel());
        recipeList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                recipeListMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(recipeList);

        ForDeletion.setText("Temporary recipe names");

        SearchButton.setText("Search");

        AllergyButton.setText("Press if you are allergic to some products");
        AllergyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AllergyButtonActionPerformed(evt);
            }
        });

        DatePanel.setForeground(java.awt.Color.lightGray);

        jLabel1.setText("Insert product name");

        jLabel2.setText("Allergic to products list:");

        CAllerygyButton.setText("Close");
        CAllerygyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CAllerygyButtonActionPerformed(evt);
            }
        });

        AddToAllleListButton.setText("Add to list");
        AddToAllleListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddToAllleListButtonActionPerformed(evt);
            }
        });

        AllergyProductArea.setEditable(false);
        AllergyProductArea.setColumns(20);
        AllergyProductArea.setRows(5);
        jScrollPane2.setViewportView(AllergyProductArea);

        javax.swing.GroupLayout AllergyPanelLayout = new javax.swing.GroupLayout(AllergyPanel);
        AllergyPanel.setLayout(AllergyPanelLayout);
        AllergyPanelLayout.setHorizontalGroup(
            AllergyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AllergyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AllergyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AllergyPanelLayout.createSequentialGroup()
                        .addComponent(AllergyField, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(AddToAllleListButton))
                    .addGroup(AllergyPanelLayout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel1))
                    .addComponent(CAllerygyButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                .addGroup(AllergyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49))
        );
        AllergyPanelLayout.setVerticalGroup(
            AllergyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AllergyPanelLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AllergyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(AllergyField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AddToAllleListButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CAllerygyButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AllergyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        AllergyPanel.setVisible(false);

        GetRecipesBu.setText("Get Recipes");
        GetRecipesBu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GetRecipesBuActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ForDeletion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(GetRecipesBu)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(CheckBoxPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(8, 8, 8)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cDateLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cSeasonLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(Searchfield, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(addRecipeButton, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                    .addComponent(SearchButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(AllergyButton))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(AllergyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(selectDateButton, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(prodInfoButton, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(46, 46, 46))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(DatePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(addRecipeButton)
                                .addGap(18, 18, 18)
                                .addComponent(SearchButton))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cDateLabel)
                                .addGap(10, 10, 10)
                                .addComponent(cSeasonLabel)
                                .addGap(8, 8, 8)
                                .addComponent(Searchfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(CheckBoxPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(AllergyButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(AllergyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                        .addComponent(ForDeletion))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(prodInfoButton)
                        .addGap(18, 18, 18)
                        .addComponent(selectDateButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(DatePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(GetRecipesBu)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        setBounds(0, 0, 816, 633);
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBox4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox4ActionPerformed

    private void jComboBoxMonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxMonthActionPerformed
        // TODO add your handling code here:
         String[] days28 = new String[29];
        for(int i=1; i<=28; i++)
        {
            days28[i] = Integer.toString(i);
        }
        String[] days29 = new String[30];
        for(int i=1; i<=29; i++)
        {
            days29[i] = Integer.toString(i);
        }
        String[] days30 = new String[31];
        for(int i=1; i<=30; i++)
        {
            days30[i] = Integer.toString(i);
        }
        String[] days31 = new String[32];
        for(int i=1; i<=31; i++)
        {
            days31[i] = Integer.toString(i);
        }
        
        String months[]= new String[]{ "January", "February", "March", "April", "May", "June", "July", "August"
                                        ,"September", "October", "November", "December"};        
        String month = ((String)jComboBoxMonth.getSelectedItem());
        int j;
        for(j=0;j<12;j++)       //run a for loop to figure out the number of the month [0-11]
        {
            if(month.equals(months[j]))
            {
                break;            
            }
        }
        int year = Integer.parseInt((String)jComboBoxYear.getSelectedItem());
        mycal.set(Calendar.YEAR,year);
        mycal.set(Calendar.DAY_OF_MONTH, 1);
        mycal.set(Calendar.MONTH, j); // to set the month we need to pass an integer([0-11] (0 is january))
        daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH); //get how many days there are in the month
        switch (daysInMonth) 
        {
            case 31:
                jComboBoxDay.setModel(new javax.swing.DefaultComboBoxModel<>(days31));
                // populate jComboBoxDay models with days31 strings
                break;
            case 30:
                jComboBoxDay.setModel(new javax.swing.DefaultComboBoxModel<>(days30));
                // populate jComboBoxDay models with days30 strings
                break;
            case 29:
                jComboBoxDay.setModel(new javax.swing.DefaultComboBoxModel<>(days29));
                // populate jComboBoxDay models with days29 strings
                break;
            case 28:
                jComboBoxDay.setModel(new javax.swing.DefaultComboBoxModel<>(days28));
                // populate jComboBoxDay models with days28 strings
                break;
            default:
                break;
        }
           
    }//GEN-LAST:event_jComboBoxMonthActionPerformed

    private void jComboBoxYearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxYearActionPerformed
        jComboBoxMonth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] 
        { "Month", "January", "February", "March", "April","May", "June", 
          "July", "August","September", "October", "November", "December" }));
        jComboBoxDay.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Day" }));
        
    }//GEN-LAST:event_jComboBoxYearActionPerformed

    private void jComboBoxDayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDayActionPerformed
        int day = Integer.parseInt(((String)jComboBoxDay.getSelectedItem()));        
        mycal.set(Calendar.DAY_OF_MONTH, day);
         
    }//GEN-LAST:event_jComboBoxDayActionPerformed

    private void setDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setDateActionPerformed
        DatePanel.setVisible(false);            
        currDate = format1.format(mycal.getTime());
        cDateLabel.setText("Current date: "+ currDate);        
        setSeason();        
    }//GEN-LAST:event_setDateActionPerformed

    private void selectDateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectDateButtonActionPerformed
        // TODO add your handling code here:
        DatePanel.setVisible(true);
    }//GEN-LAST:event_selectDateButtonActionPerformed

    private void addRecipeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addRecipeButtonActionPerformed
        // TODO add your handling code here:
        AddRecipe add = new AddRecipe();
        add.setVisible(true);
    }//GEN-LAST:event_addRecipeButtonActionPerformed

    private void prodInfoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prodInfoButtonActionPerformed
        // TODO add your handling code here:
        ProductInformationUI p = new ProductInformationUI();
        p.setVisible(true);
    }//GEN-LAST:event_prodInfoButtonActionPerformed

    private void recipeListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_recipeListMouseClicked
        // TODO add your handling code here:
               
        String RecipeTitle = (String)recipeList.getSelectedValue();
        Recipe q = new Recipe();
        q.setName(RecipeTitle);
        FullRecipe f = new FullRecipe(q,fullpath);
        f.setVisible(true);     
        
        
    }//GEN-LAST:event_recipeListMouseClicked

    private void AllergyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AllergyButtonActionPerformed
        // TODO add your handling code here:
        AllergyPanel.setVisible(true);
    }//GEN-LAST:event_AllergyButtonActionPerformed

    private void CAllerygyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CAllerygyButtonActionPerformed
        // TODO add your handling code here:
        AllergyPanel.setVisible(false);
    }//GEN-LAST:event_CAllerygyButtonActionPerformed

    private void AddToAllleListButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddToAllleListButtonActionPerformed
        // TODO add your handling code here:
        String itemText = (String)AllergyField.getText();
        String newline = "\n";
        
        AllergyProductArea.append(itemText + newline);
          
    }//GEN-LAST:event_AddToAllleListButtonActionPerformed

    private void GetRecipesBuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GetRecipesBuActionPerformed
        // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Select folder that contains all recipes");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {} 
        else {
      System.out.println("No Selection ");
    }
        File file = chooser.getSelectedFile();
        fullpath = file.getPath();            
    
        File o = new File(fullpath);
     
        File[] FileList = o.listFiles();
        for(File f : FileList)
        {
            String name = f.getName();
            int pos = name.lastIndexOf(".");
                if(pos > 0)
                {
                    name = name.substring(0, pos);
                }
            ((DefaultListModel)recipeList.getModel()).addElement(name);
        }
        
    }//GEN-LAST:event_GetRecipesBuActionPerformed
             
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddToAllleListButton;
    private javax.swing.JButton AllergyButton;
    private javax.swing.JTextField AllergyField;
    private javax.swing.JPanel AllergyPanel;
    private javax.swing.JTextArea AllergyProductArea;
    private javax.swing.JButton CAllerygyButton;
    javax.swing.JPanel CheckBoxPanel;
    javax.swing.JPanel DatePanel;
    private javax.swing.JLabel ForDeletion;
    private javax.swing.JButton GetRecipesBu;
    private javax.swing.JButton SearchButton;
    javax.swing.JTextField Searchfield;
    javax.swing.JButton addRecipeButton;
    javax.swing.JLabel cDateLabel;
    javax.swing.JLabel cSeasonLabel;
    javax.swing.JCheckBox jCheckBox1;
    javax.swing.JCheckBox jCheckBox2;
    javax.swing.JCheckBox jCheckBox3;
    javax.swing.JCheckBox jCheckBox4;
    javax.swing.JCheckBox jCheckBox5;
    javax.swing.JComboBox<String> jComboBoxDay;
    javax.swing.JComboBox<String> jComboBoxMonth;
    javax.swing.JComboBox<String> jComboBoxYear;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    javax.swing.JButton prodInfoButton;
    private javax.swing.JList<String> recipeList;
    javax.swing.JButton selectDateButton;
    javax.swing.JButton setDate;
    // End of variables declaration//GEN-END:variables
}
